/**
 * Created by navid on 1/27/15.
 * This is an agent based simulation of Dove, Hawk, Lawa lesson for simulation class.
 * Semester 931
 */

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

public class DHL {
    ArrayList<Integer> strategies = new ArrayList<Integer>();
    ArrayList<Integer> hawk = new ArrayList<Integer>();
    ArrayList<Integer> dove = new ArrayList<Integer>();
    ArrayList<Integer> lawa = new ArrayList<Integer>();
    int hawks, lawas, doves;
    private double kappa;
    private double nu;
    private double rhd, rhh, rhl, rdd, rdh, rdl, rld, rlh, rll;
    private double yieldd, yieldh, yieldl;
    private double pHawkToDove, pHawkToLawA, pDoveToHawk, pDoveToLawA,
            pLawAToHawk, pLawAToDove;

    public DHL(int total_population, double kappa, double nu,
               double rhd, double rhh, double rhl, double rdd, double rdh,
               double rdl, double rld, double rlh, double rll) {
        // Initialize population with random strategies
        for (int i = 0; i < total_population; i++) {

            int randomNum = (int) (Math.random() * 3);
            strategies.add(randomNum);
            switch (randomNum) {
                case 0:
                    hawks++;
                    break;
                case 1:
                    doves++;
                    break;
                default:
                    lawas++;
            }

        }
        this.kappa = kappa;
        this.nu = nu;
        this.rdd = rdd;
        this.rdh = rdh;
        this.rdl = rdl;
        this.rhd = rhd;
        this.rhh = rhh;
        this.rhl = rhl;
        this.rld = rld;
        this.rlh = rlh;
        this.rll = rll;
        hawk.add(hawks);
        dove.add(doves);
        lawa.add(lawas);

        update();

    }

    public static void main(String[] args) {
        DHL dhl;
        if (args != null && args.length > 0) {
            Integer total_population = Integer.parseInt(args[0]);
            Double kappa = Double.parseDouble(args[1]);
            Double nu = Double.parseDouble(args[2]);
            Double rhd = Double.parseDouble(args[3]);
            Double rhh = Double.parseDouble(args[4]);
            Double rhl = Double.parseDouble(args[5]);
            Double rdd = Double.parseDouble(args[6]);
            Double rdh = Double.parseDouble(args[7]);
            Double rdl = Double.parseDouble(args[8]);
            Double rld = Double.parseDouble(args[9]);
            Double rlh = Double.parseDouble(args[10]);
            Double rll = Double.parseDouble(args[11]);

            if (total_population != null && kappa != null && nu != null && rhd != null && rhh != null && rhl != null && rdd != null && rdh != null && rdl != null && rld != null && rlh != null && rll != null) {
                dhl = new DHL(total_population, kappa, nu, rhd, rhh, rhl, rdd, rdh, rdl, rld, rlh, rll);
            }
        }
        dhl = new DHL(100, .001, .001, 10, -5, 2.5, 2, 0, 1,
                6, -2.5, 5);
        dhl.run();
        dhl.plot();
    }

    public void update() {
        // This method is for updating transition probabilities after each step
        yieldd = kappa * (doves * rdd + hawks * rdh + lawas * rdl) * doves;
        yieldh = kappa * (doves * rhd + hawks * rhh + lawas * rhl) * hawks;
        yieldl = kappa * (doves * rld + hawks * rlh + lawas * rll) * lawas;
        pHawkToDove = nu * Math.exp(yieldd - yieldh);
        pHawkToLawA = nu * Math.exp(yieldl - yieldh);
        pDoveToHawk = nu * Math.exp(yieldh - yieldd);
        pDoveToLawA = nu * Math.exp(yieldl - yieldd);
        pLawAToHawk = nu * Math.exp(yieldh - yieldl);
        pLawAToDove = nu * Math.exp(yieldd - yieldl);

    }

    public void transition(int strategy, int index) {

        // This method runs transition function on each individual and decides
        // to which strategies it will change
        double r = Math.random();
        switch (strategy) {
            case 0:
                if (r < pHawkToDove) {
                    strategies.set(index, 1);
                    doves++;
                    dove.add(doves);
                    hawks--;
                    hawk.add(hawks);
                } else if (r < pHawkToDove + pHawkToLawA) {
                    strategies.set(index, 2);

                    lawas++;
                    lawa.add(lawas);
                    hawks--;
                    hawk.add(hawks);
                } else {
                    // in this case individual does not change its strategy
                    hawk.add(hawks);
                }

                break;
            case 1:
                if (r < pDoveToHawk) {
                    strategies.set(index, 0);

                    hawks++;
                    hawk.add(hawks);
                    doves--;
                    dove.add(doves);
                } else if (r < pDoveToHawk + pDoveToLawA) {
                    strategies.set(index, 2);

                    lawas++;
                    lawa.add(lawas);
                    doves--;
                    dove.add(doves);
                } else {
                    dove.add(doves);
                }

                break;

            // You can have any number of case statements.
            default: // Optional
                if (r < pLawAToHawk) {
                    strategies.set(index, 0);

                    hawks++;
                    hawk.add(hawks);
                    lawas--;
                    lawa.add(lawas);
                } else if (r < pLawAToDove + pLawAToHawk) {
                    strategies.set(index, 1);

                    doves++;
                    dove.add(doves);
                    lawas--;
                    lawa.add(lawas);

                } else {
                    lawa.add(lawas);
                }

        }

        // We should update transition probabilities
        update();
    }

    public void run() {
        // This method runs transition method on all individuals
        int i = 0;
        int j = strategies.size();
        while (i < j) {
            transition(strategies.get(i), i);
            i++;
        }

    }

    public void plot() {
        // This method saves hawk dove and law abiders population in time in a file then we can read it in Matlab and plot it!
        // There is no time for ploting in java!!!
        PrintStream ps;
        try {
            ps = new PrintStream("hawk.txt");
            int count = 0;
            System.out.println("---------------------------------------------------");
            System.out.println("HAWK");
            while (count < hawk.size()) {
                int num = hawk.get(count);
                System.out.println("" + num);
                ps.println(num);
                count++;
            }
            ps.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintStream ps1;
        try {
            ps1 = new PrintStream("dove.txt");
            int count = 0;
            System.out.println("---------------------------------------------------");
            System.out.println("DOVE");
            while (count < dove.size()) {
                int num = dove.get(count);
                System.out.println("" + num);
                ps1.println(num);
                count++;
            }
            ps1.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintStream ps2;
        try {
            ps2 = new PrintStream("lawa.txt");
            int count = 0;
            System.out.println("---------------------------------------------------");
            System.out.println("LAW ABIDER");
            while (count < lawa.size()) {
                int num = lawa.get(count);
                System.out.println("" + num);
                ps2.println(num);
                count++;
            }
            ps2.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
